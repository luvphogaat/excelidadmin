<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Font;

class FontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Font::paginate(20);
        return view('admin.fonts.index', ['fonts' => $products]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.fonts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // Franklin Gothic Medium Regular font name
        // dd($request->file('fontfile')->getMimeType(),$request->file('fontfile')->getClientOriginalExtension() );
        // $this->validate($request, ['fontfile' => 'file|mimes:woff,ttf,opentype,otf,sfnt,woff2|max:2048']);
        $file = $request->file('fontfile');
        // dd($file);
        $environment = env('APP_ENV');
        // dd($environment);
        // Fonts Path and Url
        if ($environment == 'local') {
            $pathFilled = public_path() . '/fonts/';
            $publicPathFilled = 'https://resources.excelidcardsolutions.com/fonts/';
        } else if ($environment == 'production') {
            $pathFilled = '/home/excevkqa/resources.excelidcardsolutions.com/fonts/';
            $publicPathFilled = 'https://resources.excelidcardsolutions.com/fonts/';
        }
        // dd($pathFilled);
        if (!is_dir($pathFilled)) {
            mkdir($pathFilled, 0777);
        }

        if($request->file('fontfile')){  
            // font file upload logic
            $filename = str_replace(' ', '%', $file->getClientOriginalName());
            $file->move($pathFilled, $file->getClientOriginalName());
            $request->merge(['fontUrl' => $publicPathFilled . $filename]);
        }
        Font::create($request->all());
        return redirect()->route('font.index')
            ->with('success','font added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
