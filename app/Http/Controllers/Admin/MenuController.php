<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // //
        // $menus = Menu::where('parent_id', '=', 0)->get();
        // $allMenus = Menu::pluck('title','id')->all();
        // return view('menu.menuTreeview',compact('menus','allMenus'));
        // $menus = Menu::with('submenus')->orderBy('orderBy', 'asc')->get();
        // Menu::with('submenus')->orderBy('orderBy')->get();
        // $menu = new \App\Models\Menu;
        // $menuItems = $menu->tree();
        // $products = Product::with('category')->paginate(20);
        // dd($menuItems);


        $menuItems = Menu::with('submenus')->orderBy('orderBy')->get();


        return view('admin.menus.index', ['menus'  => $menuItems, 'menuCount' => 6]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.menus.create', ['menus' => Menu::get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // str_replace(' ','_',$file);
        // strtolower
        // $request->request->add(['parent_id' => 0]); //add request
        $request->offsetSet('title', strtoupper($request->title));
        $request->request->add(['link' => 'products/' . str_replace(' ','_',strtolower($request->title))]); //add request
        $request->request->add(['orderBy' => '7']); //add request
        Menu::create($request->all());
        return redirect()->route('menus.index')
            ->with('success','Menu created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        //
    }



    // SubMenus Controller

    public function submenuCreate()
    {
        //
        return view('admin.menus.submenu.create');
    }
}
