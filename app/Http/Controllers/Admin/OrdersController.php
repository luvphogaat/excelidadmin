<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    //

    public function index() {
        $orders = array();
        $orders_1 = DB::table('order')->get();
        foreach (json_decode($orders_1, true) as $key => $value1) {
            $productId = $value1['product_id'];
            $referenceID = $value1['payment_reference_id'];
            $productDetails = DB::table('products')->where('id', $productId)->first();
            $categoryId = $productDetails->category_id;
            $categoryDetails = DB::table('category')->where('id', $categoryId)->first();
            $paymentDetails = DB::table('payment')->where('payment_link_reference_id', $referenceID)->first();
            $templateData = DB::table('design_templates')->where('id', $value1['design_id'])->first();
            // dd($templateData);
            $userId = $value1['user_id'];
            if ( !empty($userId) ) {
                $userDetails = DB::table('users')->where('id', $userId)->first();
                $data['user_name'] = $userDetails ? $userDetails->name: '';
                $data['user_email'] = $userDetails ? $userDetails->email : '';
            } else {
                $data['user_name'] = 'Guest';
                $data['user_email'] = 'Guest';
            }
            $data['id'] = $value1['id'];
            $data['category_name'] = $categoryDetails->category_title;
            $data['product_name'] = $productDetails->product_name;
            $data['design_id'] = $value1['design_id'];
            $templateData && $templateData->fill_template1 ? $data['design_template'] = $templateData->fill_template1: $data['design_template'] = '';
            $data['status'] = $value1['status'];
            $data['order_date'] = $value1['created_at'];
            $data['quantity'] = $value1['quantity'];
            $data['paymentReferenceID'] = $referenceID;
            $data['paymentDetails']= $paymentDetails;
            // dd($orders);
            // return view('admin.orders.index', ['orders' => $orders]);
            $data['price'] = $value1['price'];
            $data['variable_date'] = $value1['variable_data'];
            array_push($orders, $data);
        }
        return view('admin.orders.index', ['orders' => $orders]);
    }

    public function viewPaymentDetails() {
        
    }
}
