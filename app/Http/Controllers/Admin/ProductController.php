<?php

namespace App\Http\Controllers\Admin;

use Exception;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\productJobSize;
use App\Models\Category;
use App\Models\productImages;
use App\Models\jobSizeList;
use App\Models\productQtyPrice;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $products = Product::paginate(20);
        $products = Product::with(['category', 'productImages'])->paginate(20);
        return view('admin.products.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        // dd($categories);
        $jobSizes = jobSizeList::all();
        return view('admin.products.create', ['categories' => $categories, 'jobSizes' => $jobSizes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->input('category_id'));
        // $newProduct = Product::create($request->all());
            $newProduct = new Product;
            $newProduct->category_id = $request->input('category_id');
            $newProduct->product_desc = $request->input('product_desc');
            $newProduct->product_name = $request->input('product_name');
            $newProduct->product_keyword = $request->input('product_keyword');
            $newProduct->status = $request->input('status');
            $newProduct->variableData = $request->input('variableData'  );
            $newProduct->slug =  Str::slug($request->input('product_name'), "-");
            $newProduct->addonstables = $request->input('addonstables');
            $newProduct->save();

            // Adding Job Size
            $newJobSize = new productJobSize;
            $newJobSize->size_name = $request->input('size_name');
            $newJobSize->product_id = $newProduct->id;
            $newJobSize->save();

            // Adding Product Quantity and Price
            $productQtyPrice = new productQtyPrice;
            $productQtyPrice->product_id = $newProduct->id;
            $productQtyPrice->quantity = $request->input('quantity');
            $productQtyPrice->size_id = $newJobSize->id;
            $productQtyPrice->price = $request->input('price');
            $productQtyPrice->save();
            // adding Images
            $file = $request->file('product_images');
            // dd($file);
            $environment = env('APP_ENV');
            // dd($environment);
            // Fonts Path and Url
            if ($environment == 'local') {
                $pathFilled = public_path() . '/images/';
                $publicPathFilled = 'https://resources.excelidcardsolutions.com/images/products/';
            } else if ($environment == 'production') {
                $pathFilled = '/home/excevkqa/resources.excelidcardsolutions.com/images/products/';
                $publicPathFilled = 'https://resources.excelidcardsolutions.com/images/products/';
            }
            // dd($pathFilled);
            if (!is_dir($pathFilled)) {
                mkdir($pathFilled, 0777);
            }
            // dd('hi h/ere', $request->file('product_images'));
            if($file){
                // dd('reached here');
                foreach($request->file('product_images') as $image){
                    // image file upload logic
                    $filename = str_replace(' ', '%', $image->getClientOriginalName());
                    $image->move($pathFilled, $image->getClientOriginalName());
                    //$request->merge(['product_image' => $publicPathFilled . $filename]);
                    $productImage = new productImages;
                    $productImage->product_id = $newProduct->id;
                    $productImage->product_image = $publicPathFilled . $filename;
                    $productImage->save();
                }
            }
        // $user->roles()->sync($request->roles);
        return redirect()->route('products.index')->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product = Product::with(['category', 'productImages'])->findOrFail($id);
        $productsize = productJobSize::with('jobQtyPrice')->where('product_id', $id)->get();
        $jobSizes = jobSizeList::all();
        return view('admin.products.edit', ['categories' =>  Category::all(), 'product' => $product, 'productsize' => $productsize, 'jobSizes' => $jobSizes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // dd($request->all(/), $product->id);
        $file = $request->file('product_images');
        // dd($file);
        $environment = env('APP_ENV');
        // dd($environment);
        // Fonts Path and Url
        $pathFilled = '';
        $publicPathFilled = '';
        if ($environment == 'local') {
            $pathFilled = public_path() . '/images/';
            $publicPathFilled = 'https://resources.excelidcardsolutions.com/images/products/';
        } else if ($environment == 'production') {
            $pathFilled = '/home/excevkqa/resources.excelidcardsolutions.com/images/products/';
            $publicPathFilled = 'https://resources.excelidcardsolutions.com/images/products/';
        }
        // dd($pathFilled, $environment);
        if (!is_dir($pathFilled)) {
            mkdir($pathFilled, 0777);
        }
        // dd('hi h/ere', $request->file('product_images'));
        if($file){
            // dd('reached here');
            foreach($request->file('product_images') as $image){
                // image file upload logic
                $filename = str_replace(' ', '%', $image->getClientOriginalName());
                $image->move($pathFilled, $image->getClientOriginalName());
                //$request->merge(['product_image' => $publicPathFilled . $filename]);
                $productImage = new productImages;
                $productImage->product_id = $product->id;
                $productImage->product_image = $publicPathFilled . $filename;
                $productImage->save();
            }
        }
        //
        if ($request->input('size_name')) {
            $productSize = productJobSize::updateOrCreate(['size_name' => $request->input('size_name'), 'product_id' => $product->id]);
            productQtyPrice::updateOrCreate(['product_id' => $product->id, 'quantity' => $request->input('quantity'), 'size_id' => $productSize->id, 'price' => $request->input('price')]);
        } else {

        }
        $product->update($request->except(['product_images']));
        return redirect()->route('products.index')
            ->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        $product->delete();

        return redirect()->route('products.index')
            ->with('success','Product deleted successfully');
    }

    public function softDeleteProductImages($productId, $imageId) {
        $model = productImages::where('id',  $imageId )->where('product_id', $productId)->first();
        $model->delete();
        return redirect()->route('products.edit', $productId)
        ->with('success','Product Image deleted successfully');
    }

    public function updateProductSizeQty($productId, $sizeId, Request $request) {
        try {
            productQtyPrice::updateOrCreate(['product_id' => $productId, 'quantity' => $request->input('product_qtys_arr'), 'size_id' => $sizeId, 'price' => $request->input('product_prices_arr')]);
            return redirect()->route('products.edit', $productId)
            ->with('success','Product Qty/Price deleted successfully');
        } catch(Exception $e) {
// 
        }
       
    }

    public function deleteProductSizeQty($productId, $sizeId) {
        $model = productQtyPrice::where('size_id',  $sizeId )->where('product_id', $productId)->first();
        $model->delete();
        return redirect()->route('products.edit', $productId)
        ->with('success','Product Qty/Price deleted successfully');
    }
}