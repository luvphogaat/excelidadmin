<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\DesignTemplates;

class TemplateController extends Controller
{
    //
    public function index() {
        $templates = DesignTemplates::orderBy('id', 'asc')->paginate(1000);
        $templates->getCollection()->transform(function ( $value) {
            // Your code here
            $value['data'] = json_decode(preg_replace('/\s+/', '', $value['data']));
            // dd($value['data']);
            return $value;
        });
        // $templates
        // dd($templates);
        // $templates=$templates_pag->toArray();
        // foreach ($templates['data'] as $key => $value1) {
        //     $templates[$key]['data'] =  json_decode(preg_replace('/\s+/', '', $value1['data']));
        // }
        return view('admin.templates.templates', ['templates' => $templates]);
    }
}
