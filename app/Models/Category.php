<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Category extends Model
{
    use HasFactory;

    protected $table = 'category';

    protected $fillable = [
        'id', 'category_title', 'total_products' 
    ];

    public static function boot()
    {
        parent::boot();
    }

    // public function product() {
    //     return $this->belongsToMany('App\Models\Product');
    // }
}
