<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DesignTemplates extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'product_id',
        'category_id',
        'data1',
        'data2',
        'fill_template1',
        'fill_template2',
        'fontName',
        'fontUrl',
        'canvasType',
        'canvasWidth',
        'canvasHeight',
        'borderRound',
        'canvasTemplate1',
        'canvasTemplate2',
        'viewType',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
}
