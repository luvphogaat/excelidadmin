<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'link', 'status', 'orderBy'];

    // public function submenus() {

    //     return $this->hasMany('App\Models\SubMenuItem', 'parent_id', 'id');
    // }
    // public function parent() {

    //     return $this->hasOne('App\Models\Menu', 'id', 'parent_id');
    
    // }
    
    // public function children() {
    
    // return $this->hasMany('App\Models\Menu', 'parent_id', 'id');
    
    // }
    
    // public static function tree() {
    
    // return static::with(implode('.', array_fill(0, 100, 'children')))->where('parent_id', '=', '0')->get();
    
    // }

    public function submenus() {

        return $this->hasMany('App\Models\SubMenuItem', 'parent_id', 'id')->where('status', 'ENABLED');

    }

}
