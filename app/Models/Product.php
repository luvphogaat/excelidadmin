<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = [
        'id', 'category_id', 'product_desc', 'product_name', 'product_keyword', 'status', 'variableData', 'slug' ,'addonstables'
    ];

    public function category() {
        return $this->hasMany('App\Models\Category', 'id', 'category_id');
    }

    public function productImages() {
        return $this->hasMany('App\Models\productImages');
    }

    public function priceQty() {
        return $this->hasMany('App\Models\productQtyPrices', 'product_id', 'id');
        
    }
}
