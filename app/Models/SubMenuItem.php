<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubMenuItem extends Model
{
    use HasFactory;

    protected $table = 'sub_menu_items';

    protected $fillable = ['title','parent_id', 'link', 'status'];

    public function menu()
    {
        return $this->belongsTo('App\Models\Menu', 'id', 'parent_id');
    }
}
