<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jobSizeList extends Model
{
    use HasFactory;

    protected $table = 'job_size_lists';

    protected $fillable = [
        'id', 'job_size_name'
    ];


}
