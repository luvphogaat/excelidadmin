<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class productImages extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $softDelete = true;
    
    protected $table = 'product_images';

    protected $fillable = [
        'id', 'product_id', 'product_image' 
    ];

}
