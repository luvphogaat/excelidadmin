<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class productJobSize extends Model
{
    use HasFactory;

    protected $table = 'job_size';

    protected $fillable = [
        'id', 'size_name', 'product_id'
    ];

    public function jobQtyPrice() {
        return $this->hasMany('App\Models\productQtyPrice', 'size_id', 'id');
    }
}
