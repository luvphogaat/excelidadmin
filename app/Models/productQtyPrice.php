<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class productQtyPrice extends Model
{
    use HasFactory;

    
    protected $table = 'product_qty_size_prices';

    protected $fillable = [
        'id', 'product_id', 'quantity', 'size_id', 'price'
    ];
}
