<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->string('session_id', 255);
            $table->bigInteger('user_id')->nullable();
            $table->longText('data');
            $table->string('quantity', 255);
            $table->double('price',  8, 2);
            $table->bigInteger('product_id');
            $table->string('image_file_path', 255)->nullable();
            $table->string('text_file_path', 255)->nullable();
            $table->string('variable_data', 255);
            $table->bigInteger('design_id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
