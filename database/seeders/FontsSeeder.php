<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FontsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('fonts')->insert([
            'fontName' => 'Franklin Gothic Medium Regular',
            'fontUrl' => 'http://resources.excelidcardsolutions.com/fonts/Franklin%20Gothic%20Medium%20Regular.ttf'
        ]);
        DB::table('fonts')->insert([
            'fontName' => 'Arial',
            'fontUrl' => ''
        ]);
    }
}
