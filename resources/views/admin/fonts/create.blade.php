@extends('templates.main')

@section('content')
<div class="container">
    <h1>Add New font</h1>
    <div class="card" style="padding:10px">
        <form method="post" action="{{ route('font.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Font Name</label>
                <input name="fontName" type="text" class="form-control @error('fontName') is-invalid  @enderror" id="fontName"
                    aria-describedby="fontName" value="{{ old('fontName') }}" required>
                @error('fontName')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            <div class="mb-3">
                <label class="form-label" for="status">
                    System Font
                </label>
                <select name="status" class="form-select" onchange="change_type()" id="systemfont" required>
                    <option value="YES">Yes</option>
                    <option value="NO" selected>No</option>
                </select>
            </div>


            <div class="mb-3" id="uploadFontfile">
                <label for="name" class="form-label">Font File</label>
                <input name="fontfile" type="file" class="form-control @error('fontfile') is-invalid  @enderror" id="fontfile"
                    aria-describedby="fontfile" value="{{ old('price') }}">
                @error('fontfile')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

           
            <button type="submit" class="btn btn-primary">Add Font</button>
        </form>
    </div>
</div>
<script>
    function change_type() {
        var type =$("#systemfont").val();

        if(type == 'NO'){
            $("#uploadFontfile").show();
            $("#fontfile").attr('requred', true);
        }else{
            $("#uploadFontfile").hide();
            $("#fontfile").attr('requred', false);
        }
    }
</script>
@endsection