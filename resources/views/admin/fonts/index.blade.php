@extends('templates.main')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="float-start">Fonts</h1>
            <a class="btn btn-sm btn-success float-end" href="{{ route('font.create') }}" role="button">Add New Font</a> 
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="card table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Font Name</th>
                    <th>Font url</th>
                    <th>Font Creation Date</th>
                    <th>Font Updation Date</th>
                    <th class="text-center">#</th>
                </tr>
            </thead>
            <tbody>
                @foreach($fonts as $key => $font)
                {{-- {{ dd($product->category[0]->category_title) }}  --}}
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td style="width:200px">{{ $font->fontName }}</td>
                    <td>@if (!empty($font->fontUrl)) {{ $font->fontUrl }} @else System Font @endif</td>
                    <td style="width:140px">{{ $font->created_at }}</td>
                    <td style="width:140px">{{ $font->updated_at }}</td>
                    <td class="text-center" style="width:150px">
                        {{-- <a class="btn btn-sm btn-primary" href="{{ route('font.edit', $font->id) }}" role="button">Edit</a> 
                        <button type="button" class="btn btn-sm btn-danger" onclick="event.preventDefault();document.getElementById('delete-user-form-{{$font->id}}').submit();">
                            Delete
                        </button>
                        <form id="delete-user-form-{{$font->id}}" action="{{route('font.destroy', $font->id )}}" method="POST" style="display:none;">
                        @csrf
                        @method("DELETE")
                        </form> --}}
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
        {{ $fonts->links() }}
    </div>
</div>
@endsection