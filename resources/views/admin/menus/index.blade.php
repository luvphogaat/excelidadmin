@extends('templates.main')
@section('content')
<style>
    /* th, td {
          border: 1px solid black;
    } */
    th, td {
        padding: 15px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="float-start">Menus</h1>
            <a class="btn btn-sm btn-outline-success float-end" href="{{ route('submenus.create') }}"
                role="button">Create Sub Menu</a>
            {{-- @if ($menuCount < 6) --}}
            <a class="btn btn-sm btn-outline-success float-end" href="{{ route('menus.create') }}" role="button"
                style="margin-right:10px;">Create Menu</a>
            {{-- @endif --}}
        </div>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <div class="card">
        <table class="table">
            <thead>
                {{-- <th>#</th> --}}
                <td class="text-center">MENU NAME</td>
            </thead>
            <tbody>
                @foreach ($menus as $item)
                <tr>
                    {{-- <th>#</th> --}}
                    <td>
                        <label class="text-capitalize fw-bold">{{ $item->title }}</label>&nbsp;
                        @if ($item->status == 'ENABLED')
                        <span class="badge  bg-success">{{ $item->status }}</span>
                        @else
                        <span class="badge  bg-danger">{{ $item->status }}</span>
                        @endif
                        <table style="width:100%;margin-top:10px;margin-bottom: 10px;" class="table">
                            <tbody>
                                @foreach($item->submenus as $submenu)
                                <tr>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td> ->
                                        <label>{{ $submenu->title }}</label> &nbsp;
                                        @if ($submenu->status == 'ENABLED')
                                        <span class="badge  bg-success">{{ $submenu->status }}</span>
                                        @else
                                        <span class="badge  bg-danger">{{ $submenu->status }}</span>
                                        @endif
                                        {{-- @if (count($submenu->children) > 0)
                                        <table style="width:100%;margin-top:10px;margin-bottom: 10px;" class="table">
                                            <tbody>
                                                @foreach($submenu->children as $submenu1)
                                                <tr>
                                                    <td style="width: 50px;">&nbsp;</td>
                                                    <td>
                                                        -> <label>{{ $submenu1->title }}</label>&nbsp;
                                                        @if ($submenu1->status == 'ENABLED')
                                                        <span class="badge  bg-success">{{ $submenu1->status }}</span>
                                                        @else
                                                        <span class="badge  bg-danger">{{ $submenu1->status }}</span>
                                                        @endif
                                                    </td>
                                                    {{-- <td> --}}
                                                    {{-- /toggling enabled and disabled --}}
                                                    {{-- <div class="form-check form-switch">
                                                            <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                                                        </div> --}}
                                                    {{-- </td> --}}
                                                {{-- </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @endif --}}
                                    {{-- <td style="width:100px"> --}}
                                    </td>
                                    {{-- <td> --}}
                                    {{-- /toggling enabled and disabled --}}
                                    {{-- <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                                        </div> --}}
                                    {{-- </td> --}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </td>
                    @endforeach
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection