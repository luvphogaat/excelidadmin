@extends('templates.main')

@section('content')
<div class="container">
    <h1>Create New SubMenu</h1>
    <div class="card" style="padding:10px">
        <form method="post" action="{{ route('submenus.store') }}">
            @csrf
            <div class="mb-3">
                <label for="title" class="form-label">Menu Title</label>
                <input name="title" type="text" class="form-control @error('title') is-invalid  @enderror" id="title"
                    aria-describedby="title" value="{{ old('title') }}">
                @error('title')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            {{-- <div class="mb-3">
                <label for="link" class="form-label">Link</label>
                <input name="link" type="text" value="{{ 'products/' }}" class="form-control @error('link') is-invalid  @enderror" id="link"
                    aria-describedby="link" value="{{ old('link') }}">
                @error('link')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div> --}}

            <div class="mb-3">
                <label class="form-label" for="parent_id">
                    Category
                </label>
                <select name="parent_id" class="form-select" id="parent_id">
                    @foreach($menus as $menu)
                        <option value={{ $menu->id }}>{{$menu->title}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label class="form-label" for="status">
                    Status
                </label>
                <select name="status" class="form-select">
                    <option value="ENABLED" selected>ENABLED</option>
                    <option value="DISABLED">DISABLED</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection