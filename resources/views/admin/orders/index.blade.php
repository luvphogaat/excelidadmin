@extends('templates.main')
@section('content')
<style>
     /* th, td {
          border: 1px solid black;
    }
    th, td {
        padding: 15px;
    } */
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h1 class="float-start">Orders</h1>
            {{-- <a class="btn btn-sm btn-success float-end" href="{{ route('users.create') }}" role="button">Create</a>  --}}
        </div>
    </div>
    <div class="card p-2">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>User Name</th>
                    <th>User Email</th>
                    <th>Category Name</th>
                    <th>Product Name</th>
                    <th class="text-center">Design Selected</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Actions</th>
                    <th class="text-center">Payment Reference ID</th>
                    <th class="text-center">Payment status</th>
                    <th class="text-center">Payment Date</th>
                    <th class="text-center">Payment Amount</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $key => $order)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $order['user_name'] }}</td>
                    <td>{{ $order['user_email'] }}</td>
                    <td>{{ $order['category_name'] }}</td>
                    <td>{{ $order['product_name'] }}</td>
                   
                    <td class="text-center">
                        @if (!empty($order['design_id']))
                            <img src="{{ $order['design_template'] }}" style="width: 200px;height: auto" />
                        @endif
                    </td>
                    <td class="text-center">
                        @if ($order['status'] == 0) <span class="badge bg-danger">Pending</span>
                        @elseif ($order['status'] == 1) <span class="badge bg-primary">In Progress</span>
                        @elseif ($order['status'] == 2) <span class="badge bg-success">Completed</span>
                        @endif
                    </td> 
                    <td class="text-center">
                        {{-- <a class="btn btn-sm btn-primary" href="{{ route('users.edit', $user->id) }}" role="button">Edit</a> --}}
                        @if ($order['status'] == 0) 
                            {{-- <form id="delete-user-form-{{$user['id']}}" action="{{route('users.destroy', $user->id )}}" method="POST" style="display:none;">
                            @csrf
                            @method("DELETE")
                            </form> --}}
                            <a class="btn btn-sm btn-primary" href="#" role="button">Start Order</a>
                        @elseif ($order['status'] == 1) 
                            <a class="btn btn-sm btn-success" href="#" role="button">Finish Order</a>
                        @endif
                       {{-- <button type="button" class="btn btn-sm btn-danger" onclick="event.preventDefault();document.getElementById('delete-user-form-{{$user->id}}').submit();">
                            Delete
                        </button> --}}
                        {{-- <form id="delete-user-form-{{$user['id']}}" action="{{route('users.destroy', $user->id )}}" method="POST" style="display:none;">
                        @csrf
                        @method("DELETE")
                        </form> --}}
                    </td>
                    <td >
                        @if (!empty($order['paymentReferenceID']))
                        {{ $order['paymentReferenceID'] }}
                    @else
                        ----
                    @endif
                    </td>
                    <td class="text-center" style="text-transform: uppercase;">
                        @if (!empty($order['paymentDetails']->payment_link_status))
                            {{ $order['paymentDetails']->payment_link_status }}
                        @else
                            Pending
                        @endif
                    </td>
                    <td class="text-center">
                        @if (!empty($order['paymentDetails']->updated_at))
                            {{ $order['paymentDetails']->updated_at }}
                        @else
                            -----
                        @endif
                    </td>
                    <td class="text-center">
                        @if (!empty($order['paymentDetails']->amount))
                            Rs. {{ $order['paymentDetails']->amount }}
                        @else
                            -----
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{-- {{ $orders->links() }} --}}
    </div>
</div>
{{-- <script>
    $(function() {
      $('.toggle-class').change(function() {
          var status = $(this).prop('checked') == true ? 1 : 0; 
          var user_id = $(this).data('id'); 
           
          $.ajax({
              type: "GET",
              dataType: "json",
              url: '/changeStatus',
              data: {'status': status, 'user_id': user_id},
              success: function(data){
                console.log(data.success)
              }
          });
      })
    })
  </script> --}}
@endsection