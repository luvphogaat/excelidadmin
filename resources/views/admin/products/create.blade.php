@extends('templates.main')

@section('content')
<div class="container">
    <h1>Create New Product</h1>
    <div class="card" style="padding:10px">
        <form method="post" action="{{ route('products.store') }}">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Product Name</label>
                <input name="product_name" type="text" class="form-control @error('product_name') is-invalid  @enderror" id="product_name"
                    aria-describedby="product_name" value="{{ old('product_name') }}">
                @error('product_name')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            <div class="mb-3">
                <label for="name" class="form-label">Product Keywords</label>
                <input name="product_keyword" type="text" class="form-control @error('product_keyword') is-invalid  @enderror" id="product_keyword"
                    aria-describedby="product_keyword" value="{{ old('product_keyword') }}">
                @error('product_keyword')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            <div class="mb-3">
                <label for="name" class="form-label">Product Description</label>
                <input name="product_desc" type="text" class="form-control @error('product_desc') is-invalid  @enderror" id="product_desc"
                    aria-describedby="product_desc" value="{{ old('product_desc') }}">
                @error('product_desc')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            <div class="mb-3">
                <label class="form-label" for="category_id">
                    Category
                </label>
                <select name="category_id" class="form-select" id="category_id">
                    @foreach($categories as $category)
                        <option value={{ $category->id }}>{{$category->category_title}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label for="quantity" class="form-label">Product Quanity</label>
                <input name="quantity" type="number" class="form-control @error('quantity') is-invalid  @enderror" id="quantity"
                    aria-describedby="quantity" value="{{ old('quantity') }}">
                @error('quantity')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            <div class="mb-3">
                <label for="price" class="form-label">Product Price</label>
                <input name="price" type="number" min="0.00" step="1.00" class="form-control @error('price') is-invalid  @enderror" id="price"
                    aria-describedby="price" value="{{ old('price') }}">
                @error('price')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            <div class="mb-3">
                <label class="form-label" for="size_name">
                    Product Size
                </label>
                <select name="size_name" class="form-select" id="size_name">
                    <option value=""></option>
                    @foreach($jobSizes as $jobSize)
                        <option value="{{ $jobSize->job_size_name }}">{{$jobSize->job_size_name}}</option>
                    @endforeach
                </select>
            </div>

           <div class="mb-3">
                <label class="form-label" for="variableData">
                Has System Designs
                </label>
                <select name="variableData" class="form-select">
                    <option value="0" selected>No</option>
                    <option value="1">Yes</option>
                </select>
            </div>

            <div class="mb-3">
                <label class="form-label" for="status">
                    Status
                </label>
                <select name="status" class="form-select">
                    <option value="ACTIVE" selected>ACTIVE</option>
                    <option value="INACTIVE">INACTIVE</option>
                </select>
            </div>
            <div class="mb-3">
                <label class="form-label" for="status">
                   Product Images
                </label>
                <br />
                <input type="file" size="60" name="product_images[]"/>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection