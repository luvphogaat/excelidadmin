@extends('templates.main')

@section('content')
<div class="container">
    <h1>Edit Product</h1>
    <div class="card" style="padding:10px">
        <form method="POST" action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="name" class="form-label">Product Name</label>
                <input name="product_name" type="text" class="form-control @error('product_name') is-invalid  @enderror"
                    id="product_name" aria-describedby="product_name" value="{{ $product->product_name }}">
                @error('product_name')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            <div class="mb-3">
                <label for="name" class="form-label">Product Keywords</label>
                <input name="product_keyword" type="text"
                    class="form-control @error('product_keyword') is-invalid  @enderror" id="product_keyword"
                    aria-describedby="product_keyword" value="{{ $product->product_keyword }}">
                @error('product_keyword')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>
            <div class="mb-3">
                <label for="name" class="form-label">Product Description</label>
                <input name="product_desc" type="text" class="form-control @error('product_desc') is-invalid  @enderror"
                    id="product_desc" aria-describedby="product_desc" value="{{ $product->product_desc }}">
                @error('product_desc')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            
            <div class="row" style="border: 1px solid; padding: 5px; margin: 2px;">
                <h5 style="text-align: center">Add New Product Qty and Price</h5>
                <div class="mb-3 col">
                    <label class="form-label" for="size_name">
                        Product Size
                    </label>
                    <select name="size_name" class="form-select" id="size_name">
                        <option value=""></option>
                        @foreach($jobSizes as $jobSize)
                        <option value="{{ $jobSize->job_size_name }}">{{$jobSize->job_size_name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="mb-3 col">
                    <label for="name" class="form-label">Product Quantity</label>
                    <input name="quantity" type="number" class="form-control @error('quantity') is-invalid  @enderror"
                        id="quantity" aria-describedby="quantity" value="{{ $product->quantity }}">
                    @error('quantity')
                    <span class="invalid-feedback" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>
                <div class="mb-3 col">
                    <label for="name" class="form-label">Product Price</label>
                    <input name="price" type="number" min="0.00" step="1.00"
                        class="form-control @error('price') is-invalid  @enderror" id="price" aria-describedby="price"
                        value="{{ $product->price }}">
                    @error('price')
                    <span class="invalid-feedback" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>
            </div>
            <div class="mb-3">
                <label class="form-label" for="category_id">
                    Category
                </label>
                <select name="category_id" class="form-select" id="category_id">
                    @foreach($categories as $category)
                    <option value={{ $category->id }} {{ $product->category_id == $category->id ? 'selected' : '' }}>
                        {{$category->category_title}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label class="form-label" for="status">
                    Status
                </label>
                <select name="status" class="form-select">
                    <option value="ACTIVE" selected {{ $product->status == 'ACTIVE' ? 'selected' : '' }}>ACTIVE</option>
                    <option value="INACTIVE" {{ $product->status == 'INACTIVE' ? 'selected' : '' }}>INACTIVE</option>
                </select>
            </div>

            <div class="mb-3">
                <label class="form-label" for="status">
                    Images
                </label>
                <br />
                <div class="container-fluid">
                    <div class="row">
                        @foreach($product->productImages as $image)
                        <div class="col-4">
                            <a
                                href="{{ route('admin.product.image.delete', [ 'id' => $product->id, 'imageId' => $image->id]) }}"><i
                                    class="fa fa-times" aria-hidden="true"
                                    style="position: absolute; font-size: 25px; color: red"></i></a>
                            <div class="container-fluid">
                                <img src="{{ $image->product_image }}" width="300" />
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <input type="file" size="60" name="product_images[]" />
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ route('products.index') }}" type="button" class="btn btn-secondary">Back</a>
        </form>
       
        <div class="mb-3 mt-5">
            <hr/>
            <h4 style="text-align:center">Product Qty and Prices Updation</h4>
            @foreach($productsize as $size)
            <hr />
            <label for="name" class="form-label">Product Size : </label>
            <label>{{ $size->size_name }}</label>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <td>Quantity</td>
                        <th>Price</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($size->jobQtyPrice as $key => $prices)
                    <form method="POST" action="{{ route('admin.product.size.update', [$product->id, $size->id]) }}">
                        @csrf
                        @method('PUT')
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>
                                <input type="text" class="form-control @error('product_quantity') is-invalid  @enderror"
                                    id="product_prices" name="product_qtys_arr" aria-describedby="product_name" value="{{ $prices->quantity }}">
                            </td>
                            <td>
                                <input type="text" class="form-control @error('product_price') is-invalid  @enderror"
                                    id="product_prices" name="product_prices_arr" aria-describedby="product_name" value="{{ $prices->price }}">
                            </td>
                            <td>
                                {{-- <a href="{{ route('admin.product.size.update', [ 'id' => $product->id, 'sizeId' => '1']) }}">
                                    <i class="fa fa-tick" aria-hidden="true" style="position: absolute; font-size: 25px; color: red"></i>
                                </a>
                                <a href="{{ route('admin.product.size.delete', [ 'id' => $product->id, 'sizeId' => '2']) }}">
                                    <i class="fa fa-tick" aria-hidden="true" style="position: absolute; font-size: 25px; color: red"></i>
                                </a> --}}
                                <button class="btn btn-primary" type="submit">Update</i></button>
                                <a href="{{ route('admin.product.size.delete', [ 'id' => $product->id, 'sizeId' => '2']) }}">
                                    Delete
                                </a>
                            </td>
                        </tr>
                    </form>
                    @endforeach
                </tbody>
            </table>
            @endforeach
        </div>
    </div>
</div>
@endsection