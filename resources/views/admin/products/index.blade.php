@extends('templates.main')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="float-start">Products</h1>
            <a class="btn btn-sm btn-success float-end" href="{{ route('products.create') }}" role="button">Add New Product</a> 
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="card table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <td>Image</td>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>Product Desc</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">#</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $key => $product)
                {{-- {{ dd($product->category[0]->category_title) }}  --}}
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>
                        @if (!empty($product->productImages[0]->product_image))
                            <img src="{{ $product->productImages[0]->product_image }}" style="height:50px" />
                        @else
                            <img src="{{ asset('img/no-img.png') }}" style="height:50px" />
                        @endif
                    </td>
                    <td>{{ $product->product_name }}</td>
                    <td>{{ $product->category[0]->category_title }}</td>
                    <td>{{ $product->product_desc }}</td>
                    <td class="text-center" style="width:100px">
                        @if ($product->status == 'ACTIVE')
                        <span class="badge  bg-success">{{ $product->status }}</span>
                        @else
                        <span class="badge  bg-danger">{{ $product->status }}</span>
                        @endif
                    </td>
                    <td class="text-center" style="width:150px">
                        <a class="btn btn-sm btn-primary" href="{{ route('products.edit', $product->id) }}" role="button">Edit</a> 
                        <button type="button" class="btn btn-sm btn-danger" onclick="event.preventDefault();document.getElementById('delete-user-form-{{$product->id}}').submit();">
                            Delete
                        </button>
                        <form id="delete-user-form-{{$product->id}}" action="{{route('products.destroy', $product->id )}}" method="POST" style="display:none;">
                        @csrf
                        @method("DELETE")
                        </form>
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
        {{ $products->links() }}
    </div>
</div>
@endsection