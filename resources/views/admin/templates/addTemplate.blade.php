@extends('templates.main')
@section('baseLink')
    <base href="/admin/designGenerator/">
@endsection
@section('styles')
    <link rel="stylesheet" href="{{ asset('designGenerator/styles.492d74dc4e6ff3d308c6.css') }}">
    <link href="{{ asset('designGenerator/assets/css/animate.min.css" rel="stylesheet') }}" />

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{ asset('designGenerator/assets/css/light-bootstrap-dashboard.css') }}" rel="stylesheet" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
@endsection
@section('content')
<div class="container-fluid">
    {{-- <div class="container-fluid">
        <div class="card" style="padding:0px 10px">
            <h3>CREATE NEW TEMPLATE</h3>
        </div>
    </div> --}}
</div>
<app-root></app-root>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('designGenerator/runtime.80ab492fe3d778817936.js') }}"></script>
<script type="text/javascript" src="{{ asset('designGenerator/es2015-polyfills.57f55fa9d54ee176de98.js') }}"></script>
<script type="text/javascript" src="{{ asset('designGenerator/polyfills.7ff3fc35f9bdbaf81d86.js') }}"></script>
<script type="text/javascript" src="{{ asset('designGenerator/main.f6093182df4af9b2482d.js') }}"></script>

@endsection
