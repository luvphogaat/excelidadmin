@extends('templates.main')
@section('content')
<div class="container">
    <h1>Templates</h1>
    {{--  --}}
    {{-- <div class="container-fluid">
        <a class="btn btn-primary" href="{{ route('admin.newtemplate') }}">
        <i class="fa fa-plus" aria-hidden="true"></i> Add New Design
        </a>
    </div>
    <div class="container-fluid">
        Showing {{ $templates->currentPage() }} of {{ $templates->lastPage() }} (Total : {{ $templates->total() }})
    </div> --}}
    <div class="row">
        <div class="col-12">
            <span class="float-start">
                Showing {{ $templates->currentPage() }} of {{ $templates->lastPage() }} (Total : {{ $templates->total() }})
            </span>
            <a class="btn btn-sm btn-primary float-end" href="{{ route('admin.newtemplate') }}" role="button">Add New Design</a> 
        </div>
    </div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                @foreach ($templates as $template)
                {{-- {{  dd(json_decode(preg_replace('/\s+/', '', $template->data))->templateImg) }} --}}
                {{-- {{ dd( $template) }} --}}
                <div class="col-4 p-2">
                    <div class="card">
                        <div class="card-body">
                            <img src="{{ $template->fill_template1 }}" style="width:100%; height:auto" />
                            <div class="row mt-2">
                                <div class="col-12 d-grid">
                                    <a class="btn btn-outline-primary" href={{ route('admin.edittemplate', $template->id)}}>View/Edit Template</a>
                                </div>
                                {{-- <div class="col-6 d-grid gap-2">
                                    <a class="btn btn-outline-danger">Delete</a>
                                </div> --}}
                            </div>
                        </div>
                      </div>
                </div>
                @if ($template->viewType == 2)
                    <div class="col-4 p-2">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ $template->fill_template2 }}" style="width:100%; height:auto" />
                                <div class="row mt-2">
                                    <div class="col-12 d-grid">
                                        <a class="btn btn-outline-primary">View Template</a>
                                    </div>
                                    {{-- <div class="col-6 d-grid gap-2">
                                        <a class="btn btn-outline-danger">Delete</a>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @endforeach
                {{ $templates->links() }}
            </div>
        </div>
    </div>
</div>
@endsection