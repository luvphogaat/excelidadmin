@extends('templates.main')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="float-start">Users</h1>
            <a class="btn btn-sm btn-success float-end" href="{{ route('users.create') }}" role="button">Create</a> 
        </div>
    </div>

    <div class="card">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    {{-- <th>Last LoggedIn</th>
                    <th>Status</th> --}}
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $key => $user)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td> 
                        {{-- @if ($user->roles->isEmpty())
                            <span class="badge badge-pill {{ $user->roles[0]->name == 'Admin' ? 'badge-success  bg-success' : 'badge-info  bg-info' }}">{{ $user->roles[0]->name }}</span>
                        @endif --}}
                    </td>
                    {{-- <td></td>
                    <td></td> --}}
                    {{-- <td> --}}
                        {{-- <span class="badge  bg-danger">LoggedOut</span> --}}
                    {{-- </td> --}}
                    <td>
                        <a class="btn btn-sm btn-primary" href="{{ route('users.edit', $user->id) }}" role="button">Edit</a> 
                        <button type="button" class="btn btn-sm btn-danger" onclick="event.preventDefault();document.getElementById('delete-user-form-{{$user->id}}').submit();">
                            Delete
                        </button>
                        <form id="delete-user-form-{{$user->id}}" action="{{route('users.destroy', $user->id )}}" method="POST" style="display:none;">
                        @csrf
                        @method("DELETE")
                        </form>
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
</div>
@endsection