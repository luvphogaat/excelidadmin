@extends('templates.main')

@section('content')
    <h1>Hi 
        @if (Auth::user())
        {{ Auth::user()->name }}
        @else
        <a href="{{ route('login') }}" >Login</a>
        @endif
    </h1>

@endsection