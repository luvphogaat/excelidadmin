<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Welcome') }}</title>
    @yield('baseLink')
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    <!--Dynamic StyleSheets added from a view would be pasted here-->
    @yield('styles')
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon.jpg') }}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            @if (Auth::check())
            <a class="navbar-brand" href="{{ route('admin.dashboard') }}">{{ config('app.name', 'Welcome') }}</a>
            @else
            <a class="navbar-brand" href="{{ route('index') }}">{{ config('app.name', 'Welcome') }}</a>
            @endif
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    @auth
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('admin.dashboard') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('admin.order.index') }}">Order</a>
                    </li>
                    {{-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Order
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#">Pending Order</a></li>
                            <li><a class="dropdown-item" href="#">In-Progress Order</a></li>
                            <li><a class="dropdown-item" href="#">Completed Ordere</a></li>
                        </ul>
                    </li> --}}
                    
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('admin.templates') }}">Template</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('products.index') }}">Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('menus.index') }}">Menu</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('users.index') }}">User</a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('font.index') }}">Fonts</a>
                    </li>
                    {{-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Setting
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="{{ route('users.index') }}">Profile</a></li>
                            <li><a class="dropdown-item" href="{{ route('users.index') }}">Users</a></li>
                            <li><a class="dropdown-item" href="{{ route('menus.index') }}">Menus</a></li>
                        </ul>
                    </li> --}}
                     <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('users.index') }}">Users</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('users.index') }}">Profile</a>
                    </li>
                    @endauth
                </ul>
                <div class="d-flex">
                    {{-- @if (Route::has('login')) --}}
                    <div>
                        @auth
                        <a class="btn btn-outline-danger" href="{{ url('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none">
                            @csrf
                        </form>
                        {{-- @else --}}
                        {{-- <a href="{{ route('login') }}">Login</a> --}}

                        {{-- @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                        @endif --}}
                        @endauth
                    </div>
                    {{-- @endif --}}
                </div>
            </div>
        </div>
    </nav>
    <br >
    <br >
    <main class="container-fluid">
        @yield('content')
    </main>
</body>
@yield('script')

</html>
