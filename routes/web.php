<?php

use Illuminate\Support\Facades\Route;
use Admin\UserController;
use Admin\ProductController;
use Admin\MenuController;
use Admin\SubMenuController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\TemplateController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\OrdersController;
use Admin\FontController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('index');


//Admin Routes
Route::prefix('admin')->middleware('auth')->group(function () {
    Route::group(['middleware' => 'role:Admin'], function() {
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');
        Route::get('/templates', [TemplateController::class, 'index'])->name('admin.templates');
        Route::prefix('setting')->group(function () {
            Route::get('/', [SettingController::class, 'index'])->name('admin.setting.index');
            Route::resource('/users', UserController::class);
        });
        Route::get('/orders', [OrdersController::class, 'index'])->name('admin.order.index');
        // Route::prefix('orders')->group(function () {
        //     Route::get('/pending', [OrdersController::class, 'pendingOrder'])->name('admin.order.pending');
        //     // Route::get('/completed', [OrdersController::class, 'pendingOrder'])->name('admin.order.completed');
        //     // Route::get('/in-progress', [OrdersController::class, 'pendingOrder'])->name('admin.order.inprogress');
        // });
        Route::any('/designGenerator/',function() { 
            View::addExtension('html', 'php');
            return view('admin.templates.addTemplate'); 
        
        })->name('admin.newtemplate');

        Route::any('/designGenerator/#/edit/{id}',function() { 
            View::addExtension('html', 'php');
            return view('admin.templates.addTemplate'); 
        
        })->name('admin.edittemplate');
        Route::get('/products/{id}/edit/{imageId}/remove', [App\Http\Controllers\Admin\ProductController::class, 'softDeleteProductImages'])->name('admin.product.image.delete');
        Route::put('/products/{id}/edit/{sizeId}/update', [App\Http\Controllers\Admin\ProductController::class, 'updateProductSizeQty'])->name('admin.product.size.update');
        Route::get('/products/{id}/edit/{sizeId}/delete', [App\Http\Controllers\Admin\ProductController::class, 'deleteProductSizeQty'])->name('admin.product.size.delete');
        Route::resource('products', ProductController::class);
        Route::resource('menus', MenuController::class);
        Route::resource('submenus', SubMenuController::class);
        Route::resource('font', FontController::class);
        Route::get('/orders/payment/view/{referenceId}', [OrdersController::class, 'viewPaymentDetails'])->name('admin.order.payment');
    });
});
